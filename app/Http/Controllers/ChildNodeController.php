<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\NodeRequest;
use App\Models\NestedSet;

/**
 * ChildNodeController class
 *
 * @author Sergey Kuprianov <sergey.kuprianow@gmail.com> <smoke>
 */
class ChildNodeController extends Controller
{
    /**
     * Get list of all root nodes
     *
     * @param NestedSet $rootNode
     *
     * @return Response
     */
    public function index(NestedSet $rootNode)
    {
        // Get all root nodes with their children
        $rootNodes = NestedSet::getRootNodes($rootNode->id);

        return response()->json($rootNodes);
    }

    /**
     * Creation of the new root node
     *
     * @param NodeRequest $request
     * @param NestedSet   $rootNode
     *
     * @return Response
     */
    public function create(NodeRequest $request, NestedSet $rootNode)
    {
        // Begin transaction
        \DB::beginTransaction();

        // Trying to create a new node
        try {
            // Create new node
            $rootNode->children()->create([
                'name' => $request->get('name')
            ]);

            // Commit DB changes
            \DB::commit();
        } catch (Exception $ex) {
            // Rollback DB changes
            \DB::rollBack();

            return response()->json([
                'status'  => 'error',
                'message' => 'Something went wrong.'
            ]);
        }

        return response()->json([
            'status' => 'success',
            'name'   => $request->get('name')
        ]);
    }

    /**
     * Show the child node
     *
     * @param NestedSet $rootNode
     * @param NestedSet $node
     *
     * @return Response
     */
    public function show(NestedSet $rootNode, NestedSet $node)
    {
        return response()->json([
            'status' => 'success',
            'id'     => $node->id,
            'name'   => $node->name,
        ]);
    }

    /**
     * Updating of the child node
     *
     * @param NodeRequest $request
     * @param NestedSet   $rootNode
     * @param NestedSet   $node
     *
     * @return Response
     */
    public function update(NodeRequest $request, NestedSet $rootNode, NestedSet $node)
    {
        // Begin transaction
        \DB::beginTransaction();

        // Trying to update the node
        try {
            // Update the node
            $node->name = $request->get('name');
            $node->save();

            // Commit DB changes
            \DB::commit();
        } catch (Exception $ex) {
            // Rollback DB changes
            \DB::rollBack();

            return response()->json([
                'status'  => 'error',
                'message' => 'Something went wrong.'
            ]);
        }

        return response()->json([
            'status' => 'success',
            'name'   => $request->get('name')
        ]);
    }

    /**
     * Removing of the child node
     *
     * @param NestedSet $rootNode
     * @param NestedSet $node
     *
     * @return Response
     */
    public function remove(NestedSet $rootNode, NestedSet $node)
    {
        // Begin transaction
        \DB::beginTransaction();

        // Trying to remove the node
        try {
            // Remove the node
            $node->delete();

            // Commit DB changes
            \DB::commit();
        } catch (Exception $ex) {
            // Rollback DB changes
            \DB::rollBack();

            return response()->json([
                'status'  => 'error',
                'message' => 'Something went wrong.'
            ]);
        }

        return response()->json([
            'status' => 'success'
        ]);
    }
}
