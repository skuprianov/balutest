<?php

/*
  |--------------------------------------------------------------------------
  | Routes File
  |--------------------------------------------------------------------------
  |
  | Here is where you will register all of the routes in an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | This route group applies the "web" middleware group to every route
  | it contains. The "web" middleware group is defined in your HTTP
  | kernel and includes session state, CSRF protection, and more.
  |
 */

Route::group(['middleware' => ['web']], function ()
{
    Route::get('/', function ()
    {
        return view('index');
    });

    Route::group(['middleware' => ['api'], 'prefix' => 'node'], function()
    {
        /**
         * "Root nodes" routes
         */
        Route::get('/',          ['as' => 'node.getAll', 'uses' => 'NodeController@index']);
        Route::get('/{node}',    ['as' => 'node.get',    'uses' => 'NodeController@show']);
        Route::post('/',         ['as' => 'node.create', 'uses' => 'NodeController@create']);
        Route::put('/{node}',    ['as' => 'node.update', 'uses' => 'NodeController@update']);
        Route::delete('/{node}', ['as' => 'node.remove', 'uses' => 'NodeController@remove']);

        /**
         * "Child nodes" routes
         */
        Route::get('/{rootNode}/children',           ['as' => 'childNode.getAll', 'uses' => 'ChildNodeController@index']);
        Route::get('/{rootNode}/children/{node}',    ['as' => 'childNode.get',    'uses' => 'ChildNodeController@show']);
        Route::post('/{rootNode}/children',          ['as' => 'childNode.create', 'uses' => 'ChildNodeController@create']);
        Route::put('/{rootNode}/children/{node}',    ['as' => 'childNode.update', 'uses' => 'ChildNodeController@update']);
        Route::delete('/{rootNode}/children/{node}', ['as' => 'childNode.remove', 'uses' => 'ChildNodeController@remove']);
    });
});
