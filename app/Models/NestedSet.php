<?php

namespace App\Models;

use Baum\Node;

/**
 * Node class
 *
 * @author Sergey Kuprianov <sergey.kuprianow@gmail.com> <smoke>
 */
class NestedSet extends Node
{
    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'nodes';

    /**
     * Order column
     *
     * @var string
     */
    protected $orderColumn = 'id';

    /**
     * Get root nodes with their children
     *
     * @param integer|null $rootNodeId
     *
     * @return array
     */
    public static function getRootNodes($rootNodeId = null)
    {
        if (is_null(self::root())) {
            return [];
        }

        if (is_null($rootNodeId)) {
            $rootNodes = self::root()->getDescendants(['id', 'parent_id', 'depth', 'name'])->toHierarchy();
        } else {
            $rootNodes = self::find($rootNodeId)->getDescendants(['id', 'parent_id', 'depth', 'name'])->toHierarchy();
        }

        return self::getStructuredRootNodes($rootNodes);
    }

    /**
     * Get structured array with nodes
     *
     * @param array $rootNodes
     *
     * @return array
     */
    private static function getStructuredRootNodes($rootNodes)
    {
        $array = [];
        $i     = 0;
        foreach ($rootNodes as $rootNode) {
            $array[$i] = [
                'text'       => $rootNode->name,
                'id'         => $rootNode->id,
                'state'      => ['expanded' => false],
                'root'       => (($rootNode->depth == 1) ? true : false),
                'rootNodeId' => $rootNode->parent_id,
            ];
            if (!is_null($rootNode->children()->get()) && count($rootNode->children()->get()) > 0) {
                $array[$i]['nodes'] = self::getStructuredRootNodes($rootNode->children()->get());
            }

            $i++;
        }

        return $array;
    }
}
