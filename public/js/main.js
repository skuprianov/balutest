$(document).ready(function ()
{
    /**
     * Initialization of the tree
     */
    getTree();

    /**
     * Open modal window
     */
    $('#nodeModal').on('shown.bs.modal', function ()
    {
        $('#name').focus();
    });

    /**
     * Close modal window
     */
    $('#nodeModal').on('hidden.bs.modal', function ()
    {
        $('input[type=text]').val('');
        $('#nodeModal').find('.form-group').removeClass('has-error');
        $('#nodeModal').find('.help-block').text('');
        $('#nodeForm .spoofingMethod').remove();
    });

    /**
     * Form submission
     */
    $('#nodeForm').on('submit', function(event)
    {
        event.preventDefault();

        // Custom form submission
        $('#nodeModal #modalActionBtn').click();
    });

    /**
     * Select node
     */
    $(document).on('nodeSelected', '#tree', function(event, data)
    {
        var nodeId     = data.id,
            rootNodeId = data.rootNodeId;

        if (data.root == true) {
            $('.rootNodeAction')
                .attr('nodeId', nodeId)
                .attr('rootNodeId', rootNodeId)
                .prop('disabled', false);
        } else {
            $('.childNodeAction')
                .attr('nodeId', nodeId)
                .attr('rootNodeId', rootNodeId)
                .prop('disabled', false);
        }

        $('.rootAndChildNodeAction')
            .attr('nodeId', nodeId)
            .attr('rootNodeId', rootNodeId)
            .prop('disabled', false);
    });

    /**
     * Unselect node
     */
    $(document).on('nodeUnselected', '#tree', function(event, data)
    {
        $('.rootNodeAction').attr('nodeId', '').attr('rootNodeId', '').prop('disabled', true);
        $('.childNodeAction').attr('nodeId', '').attr('rootNodeId', '').prop('disabled', true);
        $('.rootAndChildNodeAction').attr('nodeId', '').attr('rootNodeId', '').prop('disabled', true);
    });

    /**
     * Node actions
     */
    $('#nodeModal #modalActionBtn').on('click', function()
    {
        switch ($(this).attr('action')) {
            case 'addRootNode':
                // Make request
                makeRequest(settings.createRootNodeUrl, 'post');
                break;
            case 'editRootNode':
                $('#nodeForm').append('<input type="hidden" name="_method" value="PUT" class="spoofingMethod" />');

                // Make request
                makeRequest(settings.updateRootNodeUrl.replace('-node-', $(this).attr('nodeId')), 'post');
                break;
            case 'addChildNode':
                // Make request
                makeRequest(settings.createChildNodeUrl.replace('-rootNode-', $(this).attr('nodeId')), 'post');
                break;
            case 'editChildNode':
                $('#nodeForm').append('<input type="hidden" name="_method" value="PUT" class="spoofingMethod" />');

                // Make request
                makeRequest(
                    settings.updateChildNodeUrl
                        .replace('-rootNode-', $(this).attr('rootNodeId'))
                        .replace('-node-', $(this).attr('nodeId')),
                    'post'
                );
                break;
        }

    });
});

/**
 * Disable/Activate form
 */
function disableNodeForm(form, disableFlag)
{
    form.find('input:text, .btn').prop('disabled', disableFlag);
}

/**
 * Initialization of the tree
 */
function getTree()
{
    $.ajax({
        url: settings.getRootNodesUrl,
        method: 'get',
        statusCode: {
            200: function(data)
            {
                $('#tree').treeview(
                {
                    data: data,
                    color: "#428bca",
                    showBorder: false,
                });
            },
            500: function()
            {
                // Show error message
                alert('Something went wrong.');
            }
        }
    });

    return true;
}

/**
 * Get node data
 */
function getNodeData(url)
{
    $.ajax({
        url: url,
        method: 'get',
        statusCode: {
            200: function(data)
            {
                $('#nodeForm #name').val(data.name);
            },
            500: function()
            {
                // Show error message
                alert('Something went wrong.');
            }
        }
    });

    return true;
}

/**
 * Remove node
 */
function removeNode(url)
{
    $.ajax({
        url: url,
        method: 'delete',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        statusCode: {
            200: function(data)
            {
                // Refresh tree
                getTree();

                // Disable buttons
                $('.rootNodeAction').attr('nodeId', '').attr('rootNodeId', '').prop('disabled', true);
                $('.childNodeAction').attr('nodeId', '').attr('rootNodeId', '').prop('disabled', true);
                $('.rootAndChildNodeAction').attr('nodeId', '').attr('rootNodeId', '').prop('disabled', true);
            },
            500: function()
            {
                // Show error message
                alert('Something went wrong.');
            }
        }
    });

    return true;
}

/**
 * Making of the request
 */
function makeRequest(url, requestMethod)
{
    var form         = $('#nodeForm'),
        spinnerBlock = $('#nodeModal #spinnerBlock'),
        formData     = new FormData(form[0]);

    // Show spinner
    spinnerBlock.spin('large');

    // Disable form
    disableNodeForm(form, true);

    $.ajax({
        url: url,
        method: requestMethod,
        data: formData,
        cache: false,
        dataType: 'json',
        contentType: false,
        processData: false,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        statusCode: {
            200: function(data)
            {
                // Stop spinner
                spinnerBlock.stop();

                // Activate form
                disableNodeForm(form, false);

                if (data.status == 'success') {
                    // Refresh tree
                    getTree();

                    // Disable action buttons
                    $('.rootNodeAction').attr('nodeId', '').attr('rootNodeId', '').prop('disabled', true);
                    $('.childNodeAction').attr('nodeId', '').attr('rootNodeId', '').prop('disabled', true);
                    $('.rootAndChildNodeAction').attr('nodeId', '').attr('rootNodeId', '').prop('disabled', true);

                    // Close modal window
                    $('#nodeModal').modal('hide');
                } else {
                    alert(data.message);
                }
            },
            422: function(data)
            {
                // Stop spinner
                spinnerBlock.stop();

                // Activate form
                disableNodeForm(form, false);

                // Show error messages
                form.find('.form-group').removeClass('has-error');
                form.find('.help-block').text('');
                $.each(data.responseJSON, function(key, value)
                {
                    $('#' + key).parents('div.form-group').addClass('has-error');
                    $('#' + key).parents('div.form-group').find('.help-block').text(value[0]);
                });
            },
            500: function()
            {
                // Stop spinner
                spinnerBlock.stop();

                // Activate form
                disableNodeForm(form, false);

                // Show error message
                alert('Something went wrong.');
            }
        }
    });
}