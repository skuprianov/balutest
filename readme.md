1. cd /path/to/your/project/directory
2. git clone git@bitbucket.org:skuprianov/balutest.git
3. composer install
4. chmod -R 755 storage/ bootstrap/cache/
5. Rename ".env.example" to ".env" and change the configuration parameters
6. php artisan key:generate
7. php artisan migrate
