@extends('layout')

@section('content')
    <div class="page-header">
        <h3>BALU Test</h3>
    </div>

    <div class="row">
        <div class="col-xs-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Actions (Root nodes)</h3>
                </div>
                <div class="panel-body">
                    <button
                        class="btn btn-success"
                        data-toggle="modal"
                        data-target="#nodeModal"
                        onclick="$('#modalActionBtn').attr('action', 'addRootNode')">
                        Add root node
                    </button>
                    <button
                        class="btn btn-warning rootNodeAction"
                        data-toggle="modal"
                        data-target="#nodeModal"
                        onclick="
                            $('#modalActionBtn')
                                .attr('action', 'editRootNode')
                                .attr('nodeId', $(this).attr('nodeId'));
                            getNodeData(settings.getRootNodeUrl.replace('-node-', $(this).attr('nodeId')));
                        "
                        disabled>
                        Edit root node
                    </button>
                    <button
                        class="btn btn-danger rootNodeAction"
                        onclick="
                            if (confirm('Are you sure?')) {
                                removeNode(settings.removeRootNodeUrl.replace('-node-', $(this).attr('nodeId')));
                            }
                        "
                        disabled>
                        Remove root node
                    </button>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Nested Set</h3>
                </div>
                <div class="panel-body">
                    <div id="tree"></div>
                </div>
            </div>
        </div>
        <div class="col-xs-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Actions (Child nodes)</h3>
                </div>
                <div class="panel-body">
                    <button
                        class="btn btn-success rootAndChildNodeAction"
                        id="addChildNodeBtn"
                        nodeId=""
                        data-toggle="modal"
                        data-target="#nodeModal"
                        onclick="$('#modalActionBtn').attr('action', 'addChildNode').attr('nodeId', $(this).attr('nodeId'));"
                        disabled>
                        Add child node
                    </button>
                    <button
                        class="btn btn-warning childNodeAction"
                        data-toggle="modal"
                        data-target="#nodeModal"
                        nodeId=""
                        onclick="
                            $('#modalActionBtn')
                                .attr('action', 'editChildNode')
                                .attr('rootNodeId', $(this).attr('rootNodeId'))
                                .attr('nodeId', $(this).attr('nodeId'));
                            getNodeData(
                                settings.getChildNodeUrl
                                    .replace('-rootNode-', $(this).attr('rootNodeId'))
                                    .replace('-node-', $(this).attr('nodeId'))
                            );
                        "
                        disabled>
                        Edit child node
                    </button>
                    <button
                        class="btn btn-danger childNodeAction"
                        onclick="
                            if (confirm('Are you sure?')) {
                                removeNode(
                                    settings.removeChildNodeUrl
                                        .replace('-rootNode-', $(this).attr('rootNodeId'))
                                        .replace('-node-', $(this).attr('nodeId'))
                                );
                            }
                        "
                        disabled>
                        Remove child node
                    </button>
                </div>
            </div>
        </div>
    </div>

    @include('modalWindow')
@endsection

@section('javascripts')
<script type="text/javascript">
    var settings = {
        getRootNodesUrl:    '{{ route("node.getAll") }}',
        getRootNodeUrl:     '{{ route("node.get", ["node" => "-node-"]) }}',
        createRootNodeUrl:  '{{ route("node.create") }}',
        updateRootNodeUrl:  '{{ route("node.update", ["node" => "-node-"]) }}',
        removeRootNodeUrl:  '{{ route("node.remove", ["node" => "-node-"]) }}',
        getChildNodeUrl:    '{{ route("childNode.get", ["rootNode" => "-rootNode-", "node" => "-node-"]) }}',
        createChildNodeUrl: '{{ route("childNode.create", ["rootNode" => "-rootNode-"]) }}',
        updateChildNodeUrl: '{{ route("childNode.update", ["rootNode" => "-rootNode-", "node" => "-node-"]) }}',
        removeChildNodeUrl: '{{ route("childNode.remove", ["rootNode" => "-rootNode-", "node" => "-node-"]) }}',
    };
</script>
<script src="{{ asset('js/main.min.js') }}"></script>
@endsection