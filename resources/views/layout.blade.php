<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>BALU Test</title>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Stylesheets -->
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/bootstrap-treeview.css') }}">
        <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    </head>
    <body>
        <div class="container">
            @yield('content')
        </div>

        <!-- Javascripts -->
        <script src="{{ asset('js/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script>
        <script src="{{ asset('js/jquery-1.11.3.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap-treeview.js') }}"></script>
        <script src="{{ asset('js/spin.min.js') }}"></script>
        <script src="{{ asset('js/jquery.spin.min.js') }}"></script>
        @yield('javascripts')

    </body>
</html>
