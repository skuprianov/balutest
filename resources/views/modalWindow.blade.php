<div class="modal fade" id="nodeModal" tabindex="-1" role="dialog" aria-labelledby="nodeModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="nodeModalLabel">Node management</h4>
            </div>
            <div class="modal-body">
                <form id="nodeForm">
                    <div class="form-group">
                        <label for="name">Node name</label>
                        <input type="text" class="form-control" id="name" name="name">
                        <span class="help-block"></span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div id="spinnerBlock"></div>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="modalActionBtn" action="">Save</button>
            </div>
        </div>
    </div>
</div>