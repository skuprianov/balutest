<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

/**
 * ChildNodeControllerTest class
 *
 * @author Sergey Kuprianov <sergey.kuprianow@gmail.com> <smoke>
 */
class ChildNodeControllerTest extends TestCase
{
    /**
     * Testing of the "Get all children of root node" method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->setDefaultData();

        $this->get('/node/100001/children')
                ->seeJsonStructure([[
                    'text',
                    'id',
                    'state' => [
                        'expanded'
                    ],
                    'root',
                    'rootNodeId'
                ]]);
    }

    /**
     * Testing of the "Create child node" method
     *
     * @return void
     */
    public function testCreate()
    {
        $this->post('/node/100001/children', ['name' => 'Test name'])
                ->seeJsonStructure([
                    'status',
                    'name'
                ]);
    }

    /**
     * Testing of the "Show child node" method
     *
     * @return void
     */
    public function testShow()
    {
        $this->setDefaultData();

        $this->get('/node/100001/children/100003')
                ->seeJsonStructure([
                    'status',
                    'id',
                    'name'
                ]);
    }

    /**
     * Testing of the "Update child node" method
     *
     * @return void
     */
    public function testUpdate()
    {
        $this->setDefaultData();

        $this->put('/node/100001/children/100003', ['name' => 'Test name!!!'])
                ->seeJsonStructure([
                    'status',
                    'name'
                ]);
    }

    /**
     * Testing of the "Remove child node" method
     *
     * @return void
     */
    public function testRemove()
    {
        $this->setDefaultData();

        $this->delete('/node/100001/children/100003')
                ->seeJsonEquals([
                    'status' => 'success'
                ]);
    }

    /**
     * Set default data for testing
     *
     * @return void
     */
    private function setDefaultData()
    {
        \DB::table('nodes')->truncate();
        \DB::table('nodes')->insert([
            [
                'id'        => 100000,
                'parent_id' => null,
                'lft'       => 1,
                'rgt'       => 8,
                'depth'     => 0,
                'name'      => 'Root'
            ],
            [
                'id'        => 100001,
                'parent_id' => 100000,
                'lft'       => 2,
                'rgt'       => 5,
                'depth'     => 1,
                'name'      => 'Test name 1'
            ],
            [
                'id'        => 100002,
                'parent_id' => 100000,
                'lft'       => 6,
                'rgt'       => 7,
                'depth'     => 1,
                'name'      => 'Test name 2'
            ],
            [
                'id'        => 100003,
                'parent_id' => 100001,
                'lft'       => 3,
                'rgt'       => 4,
                'depth'     => 2,
                'name'      => 'Test name 3'
            ]
        ]);
    }
}
